/*
 * anonymize: org.nrg.dcm.test.CrossTableServiceTest
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.dcm.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.nrg.dcm.xnat.CrossTableService;
import org.nrg.dcm.xnat.EditTable;
import org.nrg.dcm.xnat.EditTableDAO;
import org.nrg.dcm.xnat.ScriptTable;
import org.nrg.dcm.xnat.ScriptTableDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.test.context.ContextConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/org/nrg/dcm/test/TestContext.xml")
public class CrossTableServiceTest {
	public void createBadDb() {
		// proj3 doesn't have a corresponding entry in the edit table
		_editTableDAO.create(this.createEditTable(new Long(1), true, "proj1owner1"));
		_editTableDAO.create(this.createEditTable(new Long(1), true, "proj1owner2"));
		_editTableDAO.create(this.createEditTable(new Long(2), true, "proj2owner1"));
		_editTableDAO.create(this.createEditTable(new Long(3), false, "proj3owner1"));
		_scriptTableDAO.create(this.createScriptTable(new Long(1), "", "proj1owner1"));
		_scriptTableDAO.create(this.createScriptTable(new Long(1), null, "proj1owner2"));
		_scriptTableDAO.create(this.createScriptTable(new Long(2), null, "proj2owner1"));
		_scriptTableDAO.create(this.createScriptTable(new Long(4), null, "proj4owner1"));
	}
	
	public void createGoodDb() {
		_editTableDAO.create(this.createEditTable(null, true, "admin"));
		_editTableDAO.create(this.createEditTable(new Long(1), true, "proj1owner1"));
		_editTableDAO.create(this.createEditTable(new Long(2), true, "proj2owner1"));
		_editTableDAO.create(this.createEditTable(null, false, "admin1"));
		_scriptTableDAO.create(this.createScriptTable(new Long(1), "", "proj1owner1"));
		_scriptTableDAO.create(this.createScriptTable(new Long(1), "v1", "proj1owner2"));
		_scriptTableDAO.create(this.createScriptTable(new Long(2), "v2", "proj2owner1"));
		_scriptTableDAO.create(this.createScriptTable(null, "", "admin"));
		_scriptTableDAO.create(this.createScriptTable(null, "v1", "admin2"));
		
	}
	
	@Transactional
	private EditTable createEditTable(Long project_id, boolean edit, String user) {
		EditTable et = new EditTable();
		et.setProjectId(project_id);
		et.setEdit(edit);
		et.setXnatUser(user);
		return et;
	}
	
	@Before
	public void setUp() {
		s = new CrossTableService();
		s.setEtDAO(_editTableDAO);
		s.setStDAO(_scriptTableDAO);
	}
	
	@Transactional
	private ScriptTable createScriptTable(Long project_id, String script, String user) {
		ScriptTable st = new ScriptTable();
		st.setProjectId(project_id);
		st.setScript(script);
		st.setXnatUser(user);
		return st;
	}
	
	@Test
	@Transactional
	public final void testSanityCheckBadDb() {
		createBadDb();
		List<Exception> problems = s.findProblems();
		assertEquals(problems.size(), 1);
	}
	
	@Test
	@Transactional
	public final void testSanityCheckGoodDb() {
		createGoodDb();
		List<Exception> problems = s.findProblems();
		assertEquals(problems.size(), 0);
	}
	
	@Test
	@Transactional
	public final void testInsertScript() {
		createGoodDb();
		s.insertScript_helper(new Long(3), "v1", "proj3owner1");
		List<ScriptTable> st = _scriptTableDAO.getByProject(new Long(3));
		assertEquals(st.size(), 1);
		List<EditTable> et = _editTableDAO.getByProject(new Long(3));
		assertEquals(et.size(), 1);
		assertTrue(et.get(0).getEdit());
		assertEquals(_editTableDAO.findProblems().size(), 0);
		assertEquals(_scriptTableDAO.findProblems().size(), 0);
	}
	
	@Autowired
	private EditTableDAO _editTableDAO;
	@Autowired
	private ScriptTableDAO _scriptTableDAO;
	private CrossTableService s;
}
