/*
 * anonymize: org.nrg.dcm.test.AnonymizeTest
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.dcm.test;


import com.google.common.io.Files;
import org.apache.commons.io.FileUtils;
import org.dcm4che2.data.*;
import org.dcm4che2.io.DicomInputStream;
import org.dcm4che2.io.DicomOutputStream;
import org.dcm4che2.iod.module.macro.Code;
import org.junit.*;
import org.nrg.dcm.Anonymize;
import org.nrg.dcm.edit.AttributeException;
import org.nrg.dcm.edit.ScriptEvaluationException;

import java.io.*;

import static org.junit.Assert.*;

public class AnonymizeTest {
    final static File   testDataDir                 = new File("src/test/data");
    final static File   dcmFile                     = new File(testDataDir, "test.dcm");
    final static File   tmpDcm                      = new File(testDataDir, "tmp.dcm");
    final static File   sampleDcmFile               = new File(testDataDir, "sample.dcm");
    final static File   anonScript                  = new File(testDataDir, "dicom.das");
    final static File   changedAnonScript           = new File(testDataDir, "changed.das");
    final static File   nonDcmFile                  = new File(testDataDir, "nonDicom.jpg");
    final static File   invalidAnonScript           = new File(testDataDir, "invalid.das");
    final static File   gskAnonScript               = new File(testDataDir, "GSK-dicom.das");
    final static File   dianAnonScript              = new File(testDataDir, "DIAN-dicom.das");
    final static String gskAnonIdentificationString = "CNDA GHF112670 deidentification v001";

    static {
        assert (testDataDir.isDirectory());
        assert (sampleDcmFile.isFile());
        assert (anonScript.isFile());
        assert (changedAnonScript.isFile());
        assert (nonDcmFile.isFile());
        assert (invalidAnonScript.isFile());
        assert (gskAnonScript.isFile());
        assert (dianAnonScript.isFile());
    }

    @Before
    public void setUp() throws Exception {
        if (!dcmFile.isFile()) {
            Files.copy(sampleDcmFile, dcmFile);
        }
    }

    @Test
    @Ignore
    public final void invalidAnonScriptTest() {
        try {
            Anonymize.anonymize(dcmFile, "testProject", "testSubject", "testSession", false, null, invalidAnonScript);
            fail("Invalid script file should have thrown an exception");
        } catch (Exception ignored) {
        }
    }

    @Test
    public final void anonymizeFileTest() {
        try {
            Anonymize.anonymize(dcmFile, "testProject", "testSubject", "testSession", false, null, anonScript);
        } catch (IOException e) {
            fail(e.getMessage());
        }
        String tag     = null;
        String project = null;
        String subject = null;
        String session = null;
        try {
            tag = getTag(Tag.DeidentificationMethod);
            project = getTag(Tag.StudyDescription);
            subject = getTag(Tag.PatientName);
            session = getTag(Tag.PatientID);
        } catch (Exception e) {
            fail();
        }
        assertEquals("Test common deidentification v001", tag);
        assertEquals("testProject", project);
        assertEquals("testSubject", subject);
        // TODO: what is precedence?
        // "testSession" was what the variable is initially assigned to but the script reformats it thus.
//		assertEquals("testSession", session);
        assertEquals("testSubject_061214", session);
    }

    @Test
    public final void anonymizeWithInputStreamScript() throws IOException, ScriptEvaluationException, AttributeException {
        DicomObject o            = readFromFile(dcmFile);
        String      scriptString = FileUtils.readFileToString(anonScript);
        assertNull(o.getString(Tag.DeidentificationMethod));
        Anonymize.anonymize(o, "testProject", "testSubject", "testSession", scriptString);
        String tag     = o.getString(Tag.DeidentificationMethod);
        String project = o.getString(Tag.StudyDescription);
        String subject = o.getString(Tag.PatientName);
        String session = o.getString(Tag.PatientID);
        assertEquals("Test common deidentification v001", tag);
        assertEquals("testProject", project);
        assertEquals("testSubject", subject);
        // TODO: what is precedence?
        // "testSession" was what the variable is initially assigned to but the script reformats it thus.
//		assertEquals("testSession", session);
        assertEquals("testSubject_061214", session);
    }

    @Test
    public final void anonymizeOnlyDICOM() {
        try {
            Anonymize.anonymize(nonDcmFile, "testProject", "testSubject", "testSession", false, null, anonScript);
            fail();
        } catch (Exception e) {

        }
    }

    @Test
    public final void nullSubjectTest() throws Exception {
        Anonymize.anonymize(dcmFile, "testProject", null, "testSession", false, null, anonScript);
    }

    @Test
    public final void nullProjectTest() {
        try {
            Anonymize.anonymize(dcmFile, null, "testSubject", "testSession", false, null, anonScript);
        } catch (IOException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public final void nullSessionTest() {
        try {
            Anonymize.anonymize(dcmFile, "testProject", "testSubject", null, false, null, anonScript);
        } catch (IOException e) {
            fail(e.getMessage());
        }
    }

    /**
     * An edit script comes with a "describe" keyword that allows the user
     * to interactively enter values.
     *
     * When run programmatically this interactivity isn't possible so
     * we test to see that running a script with interaction built in
     * ignores the interactive part and correctly changes other DICOM
     * header fields.
     *
     * @throws Exception
     */
    @Test
    public final void scriptWithVariablesTest() throws Exception {
        Anonymize.anonymize(dcmFile, "testProject", "testSubject", null, false, null, gskAnonScript);
        String tag = getTag(Tag.DeidentificationMethod);
        assertTrue(tag.equals(gskAnonIdentificationString));
    }

    @Test
    public final void anonymizeDicomObjectTest() throws IOException {
        FileInputStream     dicom_fis = new FileInputStream(dcmFile);
        BufferedInputStream bis       = new BufferedInputStream(dicom_fis);
        DicomInputStream    dis       = new DicomInputStream(bis);
        DicomObject         d         = dis.readDicomObject();
        dis.close();
        bis.close();
        dicom_fis.close();
//        dis = null;
//        bis = null;
//        dicom_fis = null;
        try {
            Anonymize.anonymize(d, "testProject", "testSubject", "testSession", anonScript);
        } catch (ScriptEvaluationException e) {
            fail();
        } catch (AttributeException e) {
            fail();
        }
        String tag = d.getString(Tag.DeidentificationMethod);
        assertEquals("Test common deidentification v001", tag);
        assertTrue(samePixelData(d));
    }

    public final boolean samePixelData(DicomObject d) throws IOException {
        FileInputStream        sample_fis      = new FileInputStream(sampleDcmFile);
        BufferedInputStream    sample_bis      = new BufferedInputStream(sample_fis);
        final DicomInputStream sample_dis      = new DicomInputStream(sample_bis);
        DicomObject            sample          = sample_dis.readDicomObject();
        String                 samplePixelData = sample.getString(Tag.PixelData);
        String                 pixelData       = d.getString(Tag.PixelData);
        return samplePixelData.equals(pixelData);
    }

    public final void writeToFile(DicomObject o) throws IOException {
        FileOutputStream  fos   = new FileOutputStream(tmpDcm);
        DicomOutputStream dos   = new DicomOutputStream(fos);
        final String      tsuid = o.getString(Tag.TransferSyntaxUID, UID.ImplicitVRLittleEndian);
        dos.writeDataset(o, tsuid);
        dos.close();
        fos.close();
    }

    public final DicomObject readFromFile(File f) throws IOException {
        DicomInputStream dis = new DicomInputStream(f);
        DicomObject      o   = dis.readDicomObject();
        dis.close();
        return o;
    }

    @Test
    public final void testAnonymizeAndAddRecord() {
        try {
            Long l1 = (long) 300000;
            Long l2 = (long) 300001;
            Anonymize.anonymize(dcmFile, "testProject", "testSubject", "testSession", true, l1, anonScript);
            Anonymize.anonymize(dcmFile, "testProject", "testSubject", "testSession", true, l2, anonScript);
            // read it back
            Code[] single = Anonymize.getCodes(dcmFile);
            assertEquals(single.length, 2);
        } catch (IOException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public final void testAddRecord() {
        try {
            final FileInputStream     sample_fis = new FileInputStream(sampleDcmFile);
            final BufferedInputStream sample_bis = new BufferedInputStream(sample_fis);
            final DicomInputStream    sample_dis = new DicomInputStream(sample_bis);
            final DicomObject         sample     = sample_dis.readDicomObject();
            final DicomObject _recorded = Anonymize.addRecord(sample,
                                                              1,
                                                              "meaning",
                                                              "designator",
                                                              "version");
            _recorded.get(Anonymize.RecordTag);
            Code[] single = Anonymize.getCodes(_recorded);
            assertEquals(single.length, 1);
            for (Code c : single) {
                DicomObject o = c.getDicomObject();
                assertEquals("meaning", o.getString(Tag.CodeMeaning));
                assertEquals("1", o.getString(Tag.CodeValue));
                assertEquals("designator", o.getString(Tag.CodingSchemeDesignator));
                assertEquals("version", o.getString(Tag.CodingSchemeVersion));
            }

            // read multiple codes
            DicomObject multipleCodes = new BasicDicomObject();
            sample.copyTo(multipleCodes);

            for (int i = 0; i < 10; i++) {
                multipleCodes = Anonymize.addRecord(multipleCodes,
                                                    i,
                                                    "meaning",
                                                    "designator",
                                                    "version");
            }

            this.writeToFile(multipleCodes);
            Code[] multiple = Anonymize.getCodes(tmpDcm);
            assertEquals(multiple.length, 10);
            for (int i = 0; i < 10; i++) {
                DicomObject o = multiple[i].getDicomObject();
                assertEquals(o.getString(Tag.CodeValue), ((Integer) i).toString());
                assertEquals(o.getString(Tag.CodeMeaning), "meaning");
                assertEquals(o.getString(Tag.CodingSchemeDesignator), "designator");
                assertEquals(o.getString(Tag.CodingSchemeVersion), "version");
            }

            // read default codes
            DicomObject defaultCoded = new BasicDicomObject();
            sample.copyTo(defaultCoded);

            for (int i = 0; i < 10; i++) {
                defaultCoded = Anonymize.addRecord(defaultCoded, i);
            }

            Code[] defaultCodes = Anonymize.getCodes(defaultCoded);
            assertEquals(defaultCodes.length, 10);
            for (int i = 0; i < 10; i++) {
                DicomObject o = defaultCodes[i].getDicomObject();
                assertEquals(o.getString(Tag.CodeValue), ((Integer) i).toString());
                assertEquals(o.getString(Tag.CodeMeaning), Anonymize.Meaning);
                assertEquals(o.getString(Tag.CodingSchemeDesignator), Anonymize.Designator);
                assertEquals(o.getString(Tag.CodingSchemeVersion), Anonymize.Version);
            }

            // make sure values that are too long fail
            DicomObject invalidDicom = new BasicDicomObject();
            sample.copyTo(invalidDicom);

            StringBuilder buffer = new StringBuilder();
            // create a Meaning string that is too long by one.
            for (int i = 0; i < Anonymize.MeaningLength + 1; i++) {
                buffer.append('x');
            }

            try {
                Anonymize.addRecord(invalidDicom,
                                    1,
                                    buffer.toString(),
                                    "designator",
                                    "version");
                fail("Should have thrown an Exception.");
            } catch (Exception e) {
            }
        } catch (IOException e) {
            fail(e.getMessage());
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public final void needAnonymizationTest() throws IOException {
        assertFalse(Anonymize.needAnonymization(anonScript, anonScript));
        assertTrue(Anonymize.needAnonymization(anonScript, changedAnonScript));
    }

    private String getTag(int tag) throws Exception {
        String              out       = null;
        FileInputStream     dicom_fis = new FileInputStream(dcmFile);
        BufferedInputStream bis       = new BufferedInputStream(dicom_fis);
        try {
            final DicomInputStream dis = new DicomInputStream(bis);
            try {
                final DicomObject o = dis.readDicomObject();
                out = o.getString(tag);
            } finally {
                dis.close();
            }
        } finally {
            bis.close();
        }
        return out;
    }

    @After
    public void tearDown() throws Exception {
        if (dcmFile.isFile()) {
            FileUtils.forceDelete(dcmFile);
        }
        if (tmpDcm.isFile()) {
            FileUtils.forceDelete(tmpDcm);
        }
    }
}
