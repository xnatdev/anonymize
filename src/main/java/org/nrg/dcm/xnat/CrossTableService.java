/*
 * anonymize: org.nrg.dcm.xnat.CrossTableService
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.dcm.xnat;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@SuppressWarnings("unused")
@Service
public class CrossTableService {
    public EditTableDAO getEtDAO() {
        return this.etDAO;
    }

    public void setEtDAO(EditTableDAO etDAO) {
        this.etDAO = etDAO;
    }

    public ScriptTableDAO getStDAO() {
        return this.stDAO;
    }

    public void setStDAO(ScriptTableDAO stDAO) {
        this.stDAO = stDAO;
    }

    /**
     * Every project in the script table must have a corresponding row in the edit table with editing enabled.
     *
     * @return A list of exceptions that occurred.
     */
    @Transactional
    public List<Exception> findProblems() {
        Set<Long>       etProjects = etDAO.getProjects();
        Set<Long>       stProjects = stDAO.getProjects();
        List<Exception> problems   = new ArrayList<>();
        if (!(stProjects.containsAll(etProjects) && stProjects.size() == etProjects.size())) {
            problems.add(new Exception("The set of projects in the script table is not the same as the edit table."));
        }
        return problems;
    }

    @Transactional
    public void insertScript_helper(Long project_id, String script, String user) {
        ScriptTable st = new ScriptTable();
        st.setProjectId(project_id);
        st.setScript(script);
        st.setXnatUser(user);
        EditTable et = new EditTable();
        et.setProjectId(project_id);
        et.setEdit(true);
        et.setXnatUser(user);
        this.stDAO.create(st);
        this.etDAO.create(et);
    }

    @Autowired
    private EditTableDAO   etDAO;
    @Autowired
    private ScriptTableDAO stDAO;
}
