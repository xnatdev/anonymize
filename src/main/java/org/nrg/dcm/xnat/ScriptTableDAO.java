/*
 * anonymize: org.nrg.dcm.xnat.ScriptTableDAO
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.dcm.xnat;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.nrg.dcm.Anonymize;
import org.nrg.dcm.ScriptTableException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class ScriptTableDAO  extends BaseDAO<ScriptTable>{

	@Transactional
	public List<ScriptTableException> findProblems() {
		List<ScriptTableException> problems = new ArrayList<>();
		if (!atLeastOneNullProject()){
			problems.add(new ScriptTableException("No site wide edit script"));
		}
		List<ScriptTable> nullScripts = this.getAllNull("script");
		if (nullScripts.size() != 0) {
			problems.add(new ScriptTableException("Null scripts found", nullScripts));
		}
		return problems;
	}
	
	public Set<Long> getProjects() {
		List<ScriptTable> all = getAll();
		Set<Long> projects = new TreeSet<>();
		for (ScriptTable t : all) {
			if (t.getProjectId() != null){
				projects.add(t.getProjectId());
			}
		}
		return projects;
	}

	public ScriptTable get(Long project) {
		return this.getMostRecentRow(project);
	}
	
	public void insertScript(Long project, String script, String user) throws ScriptTableException {
		if (script != null) {
			_service.insertScript_helper(project, script, user);
		}
		else {
			throw new ScriptTableException("The script is null");
		}
	}
	
	public void anonymize(File dicomFile, Long project_id, String projectName, String subject, String session) throws IllegalArgumentException, IOException {
    	ScriptTable siteWide = getMostRecentRow(project_id);
    	if (siteWide != null) {
    		Anonymize.anonymize(dicomFile, projectName, subject, session, true, siteWide.getId(), siteWide.getScript());
    	}
    	else {
    		throw new IOException("Could not apply site-wide script for this project.");
    	}
	}

    private boolean atLeastOneNullProject() {
        List<ScriptTable> sts = getAllNull("projectId");
        return sts.size() > 0;
    }

    @Autowired
    private CrossTableService _service;
}
