/*
 * anonymize: org.nrg.dcm.xnat.EditTableDAO
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.dcm.xnat;

import org.nrg.dcm.EditTableException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Repository
@Transactional
public class EditTableDAO extends BaseDAO<EditTable> {
	@Transactional
	public List<EditTableException> findProblems() {
		List<EditTableException> problems = new ArrayList<EditTableException>();
		if (!atLeastOneNullProject()){
			problems.add(new EditTableException("No site wide edit script setting"));
		}
		Map<Long, List<EditTable>> projectMap = this.getProjectMap();
		for (Long p : projectMap.keySet()) {
			List<EditTable> projectRows = projectMap.get(p);
			if (projectRows.size() != 0) {
				if (this.firstRowIsEnabled(projectRows)) {
					List<EditTable> sequenceViolations = this.getEnableSequenceViolations(projectRows);
					if (sequenceViolations.size() != 0) {
						for (EditTable e : sequenceViolations) {
							problems.add(new EditTableException("Row " + e.getId() + ", project " + p + " : Sequence Violation."));
						}
					}
				}
				else {
					problems.add(new EditTableException("First row of project " + p + " is not enabled."));
				}	
			}
			else {
				// this should never happen.
				problems.add(new EditTableException("Project " + p + " has no rows in the Edit Table."));
			}
		}
		return problems;
	}
	
	/**
	 * Check if oldest row corresponding to this project is enabled.
	 * @param editTables    A list of rows for a single project. Should be in ascending order by row id.
	 * @return Indicates whether oldest row for this project is enabled.
	 */
	public boolean firstRowIsEnabled(List<EditTable> editTables) {
		EditTable oldest = this.getOldestRow(editTables);
		return oldest != null && oldest.getEdit();
	}
	
	public Set<Long> getProjects() {
		List<EditTable> all = getAll();
		Set<Long> projects = new TreeSet<Long>();
		for (EditTable t : all) {
			if (t.getProjectId() != null){
				projects.add(t.getProjectId());
			}
		}
		return projects;
	}
	
	public Map<Long,List<EditTable>> getProjectMap() {
		List<EditTable> all = this.getAll();
		Map<Long,List<EditTable>> projectMap = new HashMap<Long,List<EditTable>>();
		for (EditTable e : all) {
			if (projectMap.containsKey(e.getProjectId())) {
				projectMap.get(e.getProjectId()).add(e);
			}
			else {
				List<EditTable> projectRows = new ArrayList<EditTable>();
				projectRows.add(e);
				projectMap.put(e.getProjectId(), projectRows);
			}
		}
		return projectMap;
	}

	/**
	 * A list of rows that violate invariant (3) outlined above.
	 * @param ets A list of rows for a single project sorted from oldest to latest. Should be in ascending order by id.
	 * @return A list of rows that violate invariant (3) outlined above.
	 */
	public List<EditTable> getEnableSequenceViolations(List<EditTable> ets) {
		List<EditTable> duplicates = new ArrayList<EditTable>();
		EditTable current = null;
		for (EditTable e : ets){
			if (current == null) {
				current = e;
			}
			else {
				if (e.getEdit() == current.getEdit()) {
					duplicates.add(e);
					current = null;
				}
			}
		}
		return duplicates;
	}
	
	public boolean atLeastOneNullProject() {
		List<EditTable> sts = getAllNull("projectId");
		return sts.size() > 0;
	}
	
	@Transactional
	public void enable(Long project, String user) {
		EditTable e = this.get(project);
		if (!e.edit) {
			EditTable n = new EditTable();
			n.setEdit(true);
			n.setProjectId(project);
			n.setXnatUser(user);
			this.create(n);
		}
	}
	
	@Transactional
	public void disable(Long project, String user) {
		EditTable e = this.get(project);
		if (e.edit) {
			EditTable n = new EditTable();
			n.setEdit(false);
			n.setProjectId(project);
			n.setXnatUser(user);
			this.create(n);
		}
	}

    @Transactional
	public void setEdit(Long project, String user, boolean activate) {
		if (activate){
			this.enable(project, user);
		}
		else {
			this.disable(project,user);
		}
	}

    @Transactional
	public EditTable get(Long project) {
		return this.getMostRecentRow(project);
	}
}
