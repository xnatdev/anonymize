/*
 * anonymize: org.nrg.dcm.xnat.ScriptTable
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.dcm.xnat;
import javax.persistence.Column;
import javax.persistence.Entity;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntity;
import org.nrg.framework.orm.hibernate.annotations.Auditable;

@Auditable
@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "nrg")
public class ScriptTable extends AbstractHibernateEntity {
    private static final long serialVersionUID = -8767178674267853130L;
    public Long project_id;
	public String script;
	public String xnatUser;
	
	public String getXnatUser() {
		return xnatUser;
	}
	public void setXnatUser(String user) {
		this.xnatUser = user;
	}
	public Long getProjectId() {
		return project_id;
	}
	public void setProjectId(Long project_id) {
		this.project_id = project_id;
	}
	@Column(columnDefinition = "TEXT")
	public String getScript() {
		return script;
	}
	public void setScript(String script) {
		this.script = script;
	}
}
