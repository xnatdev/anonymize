DEPRECATED- NRG Anonymize Framework
================================

The (NOW DEPRECATED) NRG anonymize framework supported the application of [DicomEdit](https://bitbucket.org/nrg/dicomedit)
anonymization scripts to DICOM studies and the associated DICOM header metadata. The [Mizer](https://bitbucket.org/xnatdcm/mizer) repo now serves that function.

Building
--------

To build the NRG anonymize framework, invoke Maven with the desired lifecycle phase.
For example, the following command cleans previous builds, builds a new jar file, 
creates archives containing the source code and JavaDocs for the library, runs the 
library's unit tests, and installs the jar into the local repository:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mvn clean install
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
